# hsi-sandbox-3-notes



## Intro
We'll be studying [Next.JS](https://nextjs.orgo/). Use this [roadmap](https://roadmap.sh/frontend?r=frontend-beginner).

## Method
- Asynchronous


## Levels

### Level 0 - Dasar Pemrograman

Materi: 
- Git & Repository
- Code Editor

Output:
- Mempunyai satu akun Gitlab
- Menginstall Code Editor (disarankan Visual Studio Code)

### Level 1 - HTML CSS JS

Materi:
- HTML
- CSS
- JS

Output:
- Peserta bisa meniru layout salah satu web


## Tips & Trik
- Ikhlas karena Allah Ta'ala
- Meniatkan untuk menuntut ilmu yang bermanfaat, dan bisa menjadikan sebagai ladang kebaikan akhirat
- Semangat dan jangan menyerah
- Tumbuhkan motivasi belajar mandiri
- Mengikuti arahan dan jangan sungkan untuk bertanya
- Fokus pada kemampuan diri sendiri
